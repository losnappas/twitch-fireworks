let btn = document.querySelector('#fire')
btn.addEventListener('click', (e) => {
	e.preventDefault()
	Twitch.ext.bits.useBits('fire-1')
})
Twitch.ext.listen(
	'broadcast',
	() => createFirework(25,187,5,1,null,null,null,null,false,true)
)
Twitch.ext.listen(
	'global',
	() => createFirework(25,187,5,1,null,null,null,null,false,true)
)
